/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar;

import java.sql.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vinicius
 */
public class FuncoesTest {
    
    public FuncoesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class Funcoes.
     */
    @Test
    public void testVerificaELimpaCaracteresEspeciais() {
        System.out.println("testVerificaELimpaCaracteresEspeciais");
        String text = "',./*\\-+";
        text = Funcoes.getInstance().verificaELimpaCharEspeciais(text);
        System.out.println(text);
    }

    
}
