/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar.TextArea;

/**
 *
 * @author vinicius
 */
public class SimpleNullTextAreaValidar implements Validar.ValidarTextArea{

    @Override
    public int getMinLength() {
        return 5;
    }

    @Override
    public boolean aceptedNullable() {
        return true;
    }
    
}
