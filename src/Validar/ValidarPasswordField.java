/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar;

/**
 *
 * @author vinicius
 */
public interface ValidarPasswordField {

    public abstract boolean isObrigatorioTerLetraMaiuscula();
    public abstract boolean isObrigatorioTerLetra();
    public abstract boolean isObrigatorioTerNumero();
    public abstract boolean isObrigatorioTerCaracteresEspeciais();
    public abstract int numeroMinimoDeCaracteres();
    public abstract int numeroMaximoDeCaracteres();
    public abstract boolean naoPodeTerLetraMaiuscula();
    public abstract boolean naoPodeTerLetra();
    public abstract boolean naoPodeTerNumero();
    public abstract boolean naoPodeTerCaracteresEspeciais();
    
}
