/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar;

import java.awt.Component;
import javax.swing.JPanel;

/**
 *
 * @author vinicius
 */
public class JValidar {
    
    private static JValidar validar = null;
    
    public static JValidar getInstance(){
        if(validar == null)validar = new JValidar();
        return validar;
    }
    
    public boolean validaCampos(JPanel panel){
        Component [] comp = panel.getComponents();
        for (int i = 0; i < comp.length; i++) {
            if(comp[i] instanceof TestValidar){
                TestValidar t = (TestValidar) comp[i];
                if(!t.valida()){
                    return false;
                }
            }
        }
        return true;
    }
}
