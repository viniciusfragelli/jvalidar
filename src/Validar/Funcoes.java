/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar;

import java.awt.Component;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import javax.swing.JPanel;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vinicius
 */
public class Funcoes {
    
    private static Funcoes funcoes = null;
    
    public static Funcoes getInstance(){
        if(funcoes == null)funcoes = new Funcoes();
        return funcoes;
    }
    
    public boolean verificaSeETelefone(String telefone){
        if(telefone.charAt(0) == '('){
            if(verificaIsNumero(telefone.substring(1, 3))){
                if(telefone.charAt(3) == ')'){
                    if(telefone.charAt(4) == ' '){
                        String aux = "";
                        int i;
                        for (i = 5; telefone.charAt(i) != '-' && i<telefone.length() ; i++) {
                            aux += telefone.charAt(i);
                        }
                        if(verificaIsNumero(aux)){
                            if(i < telefone.length()){
                                if(telefone.charAt(i) == '-'){
                                    aux = "";
                                    for (i = i + 1; i < telefone.length(); i++) {
                                        aux += telefone.charAt(i);
                                    }
                                    return verificaIsNumero(aux);
                                }else{
                                    return false;
                                }
                            }else{
                                return false;
                            }
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public String setStStringNullable(String s){
        if(s == null){
            return "NULL";
        }else{
            return "'"+s+"'";
        }
    }
    
    public String setStStringNullable(Integer s){
        if(s == null){
            return "NULL";
        }else{
            return "'"+s+"'";
        }
    }
    
    public String setStStringNullable(Float s){
        if(s == null){
            return "NULL";
        }else{
            return "'"+s.toString()+"'";
        }
    }
    
    public String formataValor(Float valor, boolean isbank){
        if(isbank){
            return setStStringNullable(valor);
        }else{
            if(valor == null)return "";
            return NumberFormat.getCurrencyInstance().format(valor).replace("R$ ", "");
        }
    }
    
    public String formataInt(Integer valor, boolean isbank){
        if(isbank){
            return setStStringNullable(valor);
        }else{
            if(valor == null)return "";
            return valor+"";
        }
    }
    
    public String formataInt(Object valor, boolean isbank){
        if(isbank){
            if(valor == null){
                return "NULL";
            }else{
                return setStStringNullable(Integer.parseInt(valor.toString()));
            }
        }else{
            if(valor == null)return "";
            return valor+"";
        }
    }
    
    public Integer formataIntParse(Object valor, boolean isbank){
        if(valor != null){
            if(valor.toString().equalsIgnoreCase("")){
                return null;
            }else{
                return Integer.parseInt(valor.toString());
            }
        }else{
            return null;
        }
    }
    
    public String formataValor(Object valor, boolean isbank){
        if(isbank){
            if(valor != null){
                return setStStringNullable(Float.parseFloat(valor.toString()));
            }else{
                return "NULL";
            }
        }else{
            if(valor == null)return "";
            return NumberFormat.getCurrencyInstance().format(valor).replace("R$ ", "");
        }
    }
    
    public final String ConverteMesIntToStringDe1A12(int k){
        String s = "";
        switch(k){
            case 1: s = "Janeiro"; break;
            case 2: s = "Fevereiro"; break;
            case 3: s = "Marco"; break;
            case 4: s = "Abril"; break;
            case 5: s = "Maio"; break;
            case 6: s = "Junho"; break;
            case 7: s = "Julho"; break;
            case 8: s = "Agosto"; break;
            case 9: s = "Setembro"; break;
            case 10: s = "Outubro"; break;
            case 11: s = "Novembro"; break;
            case 12: s = "Dezembro"; break;
            default: s = "error";
        }
        return s;
    }
    
    public final String ConverteMesIntToStringDe0A11(int k){
        String s = "";
        switch(k){
            case 0: s = "Janeiro"; break;
            case 1: s = "Fevereiro"; break;
            case 2: s = "Marco"; break;
            case 3: s = "Abril"; break;
            case 4: s = "Maio"; break;
            case 5: s = "Junho"; break;
            case 6: s = "Julho"; break;
            case 7: s = "Agosto"; break;
            case 8: s = "Setembro"; break;
            case 9: s = "Outubro"; break;
            case 10: s = "Novembro"; break;
            case 11: s = "Dezembro"; break;
            default: s = "error";
        }
        return s;
    }
    
    public int converteMesStringToIntDe0A11(String s){
        int k = -1;
        switch(s){
            case "Janeiro": k = 0; break;
            case "Fevereiro": k = 1; break;
            case "Marco": k = 2; break;
            case "Abril": k = 3; break;
            case "Maio": k = 4; break;
            case "Junho": k = 5; break;
            case "Julho": k = 6; break;
            case "Agosto": k = 7; break;
            case "Setembro": k = 8; break;
            case "Outubro": k = 9; break;
            case "Novembro": k = 10; break;
            case "Dezembro": k = 11; break;
        }
        return k;
    }
    
    public int converteMesStringToIntDe1A12(String s){
        int k = -1;
        switch(s){
            case "Janeiro": k = 1; break;
            case "Fevereiro": k = 2; break;
            case "Marco": k = 3; break;
            case "Abril": k = 4; break;
            case "Maio": k = 5; break;
            case "Junho": k = 6; break;
            case "Julho": k = 7; break;
            case "Agosto": k = 8; break;
            case "Setembro": k = 9; break;
            case "Outubro": k = 10; break;
            case "Novembro": k = 11; break;
            case "Dezembro": k = 12; break;
        }
        return k;
    }
    
    public int convertLong(long tempototal){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        int dia = (Integer.parseInt(format.format(tempototal).substring(0,2)) - 1);
        int mes = (Integer.parseInt(format.format(tempototal).substring(3,5)) - 1);
        int ano = (Integer.parseInt(format.format(tempototal).substring(6,10)) - 1970);
        int hora = (Integer.parseInt(format.format(tempototal).substring(11,13)));
        int min = (Integer.parseInt(format.format(tempototal).substring(14)));
        min += hora*60;
        min += dia*24*60;
        min += mes*40320;
        min += ano*12*40320;
        return min;
    }
    
    public final Date formataData(String data) {
        if(data == null)return null;
        if(data.equalsIgnoreCase(""))return null;
        if(data.contains("/")){
            if(data.equalsIgnoreCase("  /  /    "))return null;
            try {
                SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
                return new Date(simple.parse(data).getTime());
            } catch (ParseException ex) {
                return null;
            }
        }else{
            if(data.equalsIgnoreCase("    -  -  "))return null;
            try {
                SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
                return new Date(simple.parse(data).getTime());
            } catch (ParseException ex) {
                return null;
            }
        }
    }
    
    public final String formataAno(int k) {
        String s = k+"";
        if(s.length() == 1){
            return "000"+k;
        }else{
            if(s.length() == 2){
                return "00"+k;
            }else{
                if(s.length() == 3){
                    return "0"+k;
                }else{
                    return ""+k;
                }
            }
        }
    }
    
    public final String formataData(int k) {
        switch (k) {
            case 0:
                return "00";
            case 1:
                return "01";
            case 2:
                return "02";
            case 3:
                return "03";
            case 4:
                return "04";
            case 5:
                return "05";
            case 6:
                return "06";
            case 7:
                return "07";
            case 8:
                return "08";
            case 9:
                return "09";
            default:
                return k + "";
        }
    }
    
    public final boolean verificaIsNumero(String s){
        if(!s.equals("")){
            s = s.toUpperCase();
            for(int i = 0;i<s.length();i++){
                switch(s.charAt(i)){
                    case '0': break;
                    case '1': break;
                    case '2': break;
                    case '3': break;
                    case '4': break;
                    case '5': break;
                    case '6': break;
                    case '7': break;
                    case '8': break;
                    case '9': break;
                    default: return false;
                }
            }
            return true;
        }else{
            return false;
        }
    }
    
    public final boolean verificaSeEHoraValida(String hora){
        if(!hora.equals("")){
            if(hora.length() == 5){
                if(verificaIsNumero(hora.substring(0, 2))){
                    int h = Integer.parseInt(hora.substring(0, 2));
                    if(h >= 0 && h < 24){
                        if(hora.charAt(2) == ':'){
                            if(verificaIsNumero(hora.substring(3))){
                                int min = Integer.parseInt(hora.substring(3));
                                    if(min >= 0 && min < 60){
                                        return true;
                                    }else{
                                        return false;
                                    }
                            }else{
                                return false;
                            }
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public boolean verificaSeEmailEValido(String email){
        if(email.equals(""))return false;
        if(email.contains("@")){
            if(email.charAt(0) != '@'){
                if(email.contains(".com")){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public Float formataValorEParseFloat(String s){
        if(s.equalsIgnoreCase(""))return null;
        if(s.length() > 3){
            s = s.substring(0, s.length()-3).replace(".", "")+s.substring(s.length()-3);
        }
        return Float.parseFloat(s.replace(",", "."));
    }
    
    public final boolean verificaIsValorComercialComVirgulaValido(String s){
        if(!s.equals("")){
            s = s.toUpperCase();
            if(s.length() > 3){
                s = s.substring(0, s.length()-3).replace(".", "")+s.substring(s.length()-3);
            }
            int cont = 0;
            for(int i = 0;i<s.length();i++){
                switch(s.charAt(i)){
                    case '0': break;
                    case '1': break;
                    case '2': break;
                    case '3': break;
                    case '4': break;
                    case '5': break;
                    case '6': break;
                    case '7': break;
                    case '8': break;
                    case '9': break;
                    case '.': cont++; break; 
                    case ',': cont++; break;
                    default: return false;
                }
            }
            if(cont <= 1){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public final boolean verificaIsDataValida(String data){
        if(data.length() == 10){
            if(verificaIsNumero(data.substring(0, 2)) && data.charAt(2) == '/' && verificaIsNumero(data.substring(3,5)) && data.charAt(5) == '/' && verificaIsNumero(data.substring(6))){
                if(Integer.parseInt(data.substring(0, 2)) <= 31 && Integer.parseInt(data.substring(3,5)) <= 12){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public final String verificaELimpaCharIndesejaveisBancoEspeciais(String s){
        if(s.equals(""))return "";
        String mai = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(mai.charAt(i)){
                case 'A': aux = aux+s.charAt(i); break;
                case 'B': aux = aux+s.charAt(i); break;
                case 'C': aux = aux+s.charAt(i); break;
                case 'D': aux = aux+s.charAt(i); break;
                case 'E': aux = aux+s.charAt(i); break;
                case 'F': aux = aux+s.charAt(i); break;
                case 'G': aux = aux+s.charAt(i); break;
                case 'H': aux = aux+s.charAt(i); break;
                case 'I': aux = aux+s.charAt(i); break;
                case 'J': aux = aux+s.charAt(i); break;
                case 'K': aux = aux+s.charAt(i); break;
                case 'L': aux = aux+s.charAt(i); break;
                case 'M': aux = aux+s.charAt(i); break;
                case 'N': aux = aux+s.charAt(i); break;
                case 'O': aux = aux+s.charAt(i); break;
                case 'P': aux = aux+s.charAt(i); break;
                case 'Q': aux = aux+s.charAt(i); break;
                case 'R': aux = aux+s.charAt(i); break;
                case 'S': aux = aux+s.charAt(i); break;
                case 'T': aux = aux+s.charAt(i); break;
                case 'U': aux = aux+s.charAt(i); break;
                case 'V': aux = aux+s.charAt(i); break;
                case 'W': aux = aux+s.charAt(i); break;
                case 'X': aux = aux+s.charAt(i); break;
                case 'Y': aux = aux+s.charAt(i); break;
                case 'Z': aux = aux+s.charAt(i); break;
                case '0': aux = aux+s.charAt(i); break;
                case '1': aux = aux+s.charAt(i); break;
                case '2': aux = aux+s.charAt(i); break;
                case '3': aux = aux+s.charAt(i); break;
                case '4': aux = aux+s.charAt(i); break;
                case '5': aux = aux+s.charAt(i); break;
                case '6': aux = aux+s.charAt(i); break;
                case '7': aux = aux+s.charAt(i); break;
                case '8': aux = aux+s.charAt(i); break;
                case '9': aux = aux+s.charAt(i); break;
                case ' ': aux = aux+s.charAt(i); break;
                case '-': aux = aux+s.charAt(i); break;
                case ':': aux = aux+s.charAt(i); break;
                case ',': aux = aux+s.charAt(i); break;
                case '.': aux = aux+s.charAt(i); break;
                case '*': aux = aux+s.charAt(i); break;
                case '(': aux = aux+s.charAt(i); break;
                case ')': aux = aux+s.charAt(i); break;
                case '{': aux = aux+s.charAt(i); break;
                case '}': aux = aux+s.charAt(i); break;
                case '%': aux = aux+s.charAt(i); break;
                case '$': aux = aux+s.charAt(i); break;
                case '#': aux = aux+s.charAt(i); break;
                case '@': aux = aux+s.charAt(i); break;
                case '!': aux = aux+s.charAt(i); break;
                case '?': aux = aux+s.charAt(i); break;
                case '=': aux = aux+s.charAt(i); break;
                case '&': aux = aux+s.charAt(i); break;
                case '/': aux = aux+s.charAt(i); break;
                case '+': aux = aux+s.charAt(i); break;
                case 'Ã': aux = aux+s.charAt(i); break;
                case 'Á': aux = aux+s.charAt(i); break;
                case 'À': aux = aux+s.charAt(i); break;
                case 'Â': aux = aux+s.charAt(i); break;
                case 'Ä': aux = aux+s.charAt(i); break;
                case 'É': aux = aux+s.charAt(i); break;
                case 'Ê': aux = aux+s.charAt(i); break;
                case 'È': aux = aux+s.charAt(i); break;
                case 'Ë': aux = aux+s.charAt(i); break;
                case 'Í': aux = aux+s.charAt(i); break;
                case 'Ì': aux = aux+s.charAt(i); break;
                case 'Î': aux = aux+s.charAt(i); break;
                case 'Ï': aux = aux+s.charAt(i); break;
                case 'Õ': aux = aux+s.charAt(i); break;
                case 'Ó': aux = aux+s.charAt(i); break;
                case 'Ò': aux = aux+s.charAt(i); break;
                case 'Ô': aux = aux+s.charAt(i); break;
                case 'Ö': aux = aux+s.charAt(i); break;
                case 'Ú': aux = aux+s.charAt(i); break;
                case 'Ù': aux = aux+s.charAt(i); break;
                case 'Û': aux = aux+s.charAt(i); break;
                case 'Ü': aux = aux+s.charAt(i); break;
                case 'Ç': aux = aux+s.charAt(i); break;
                case 'Ñ': aux = aux+s.charAt(i); break;
                default: break;
            }
        }
        return aux;
    }
    
    public final String verificaELimpaCharEspeciais(String s){
        if(s.equals(""))return "";
        String mai = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(mai.charAt(i)){
                case 'A': aux = aux+s.charAt(i); break;
                case 'B': aux = aux+s.charAt(i); break;
                case 'C': aux = aux+s.charAt(i); break;
                case 'D': aux = aux+s.charAt(i); break;
                case 'E': aux = aux+s.charAt(i); break;
                case 'F': aux = aux+s.charAt(i); break;
                case 'G': aux = aux+s.charAt(i); break;
                case 'H': aux = aux+s.charAt(i); break;
                case 'I': aux = aux+s.charAt(i); break;
                case 'J': aux = aux+s.charAt(i); break;
                case 'K': aux = aux+s.charAt(i); break;
                case 'L': aux = aux+s.charAt(i); break;
                case 'M': aux = aux+s.charAt(i); break;
                case 'N': aux = aux+s.charAt(i); break;
                case 'O': aux = aux+s.charAt(i); break;
                case 'P': aux = aux+s.charAt(i); break;
                case 'Q': aux = aux+s.charAt(i); break;
                case 'R': aux = aux+s.charAt(i); break;
                case 'S': aux = aux+s.charAt(i); break;
                case 'T': aux = aux+s.charAt(i); break;
                case 'U': aux = aux+s.charAt(i); break;
                case 'V': aux = aux+s.charAt(i); break;
                case 'W': aux = aux+s.charAt(i); break;
                case 'X': aux = aux+s.charAt(i); break;
                case 'Y': aux = aux+s.charAt(i); break;
                case 'Z': aux = aux+s.charAt(i); break;
                case '0': aux = aux+s.charAt(i); break;
                case '1': aux = aux+s.charAt(i); break;
                case '2': aux = aux+s.charAt(i); break;
                case '3': aux = aux+s.charAt(i); break;
                case '4': aux = aux+s.charAt(i); break;
                case '5': aux = aux+s.charAt(i); break;
                case '6': aux = aux+s.charAt(i); break;
                case '7': aux = aux+s.charAt(i); break;
                case '8': aux = aux+s.charAt(i); break;
                case '9': aux = aux+s.charAt(i); break;
                case ' ': aux = aux+s.charAt(i); break;
                case '-': aux = aux+s.charAt(i); break;
                case ':': aux = aux+s.charAt(i); break;
                case ',': aux = aux+s.charAt(i); break;
                case '.': aux = aux+s.charAt(i); break;
                case '*': aux = aux+s.charAt(i); break;
                case '(': aux = aux+s.charAt(i); break;
                case ')': aux = aux+s.charAt(i); break;
                case '{': aux = aux+s.charAt(i); break;
                case '}': aux = aux+s.charAt(i); break;
                case '%': aux = aux+s.charAt(i); break;
                case '$': aux = aux+s.charAt(i); break;
                case '#': aux = aux+s.charAt(i); break;
                case '@': aux = aux+s.charAt(i); break;
                case '!': aux = aux+s.charAt(i); break;
                case '?': aux = aux+s.charAt(i); break;
                case '=': aux = aux+s.charAt(i); break;
                case '&': aux = aux+s.charAt(i); break;
                case '/': aux = aux+s.charAt(i); break;
                case '+': aux = aux+s.charAt(i); break;
                case 'Ã': aux = aux+s.charAt(i); break;
                case 'Á': aux = aux+s.charAt(i); break;
                case 'À': aux = aux+s.charAt(i); break;
                case 'Â': aux = aux+s.charAt(i); break;
                case 'Ä': aux = aux+s.charAt(i); break;
                case 'É': aux = aux+s.charAt(i); break;
                case 'Ê': aux = aux+s.charAt(i); break;
                case 'È': aux = aux+s.charAt(i); break;
                case 'Ë': aux = aux+s.charAt(i); break;
                case 'Í': aux = aux+s.charAt(i); break;
                case 'Ì': aux = aux+s.charAt(i); break;
                case 'Î': aux = aux+s.charAt(i); break;
                case 'Ï': aux = aux+s.charAt(i); break;
                case 'Õ': aux = aux+s.charAt(i); break;
                case 'Ó': aux = aux+s.charAt(i); break;
                case 'Ò': aux = aux+s.charAt(i); break;
                case 'Ô': aux = aux+s.charAt(i); break;
                case 'Ö': aux = aux+s.charAt(i); break;
                case 'Ú': aux = aux+s.charAt(i); break;
                case 'Ù': aux = aux+s.charAt(i); break;
                case 'Û': aux = aux+s.charAt(i); break;
                case 'Ü': aux = aux+s.charAt(i); break;
                case 'Ç': aux = aux+s.charAt(i); break;
                case 'Ñ': aux = aux+s.charAt(i); break;
                default: break;
            }
        }
        return aux;
    }
    
    public boolean verificaSeTemLetraMaiuscula(String s){
        if(s.equals(""))return false;
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A': return true;
                case 'B': return true;
                case 'C': return true;
                case 'D': return true;
                case 'E': return true;
                case 'F': return true;
                case 'G': return true;
                case 'H': return true;
                case 'I': return true;
                case 'J': return true;
                case 'K': return true;
                case 'L': return true;
                case 'M': return true;
                case 'N': return true;
                case 'O': return true;
                case 'P': return true;
                case 'Q': return true;
                case 'R': return true;
                case 'S': return true;
                case 'T': return true;
                case 'U': return true;
                case 'V': return true;
                case 'W': return true;
                case 'X': return true;
                case 'Y': return true;
                case 'Z': return true;
                case 'Ã': return true;
                case 'Á': return true;
                case 'À': return true;
                case 'Â': return true;
                case 'Ä': return true;
                case 'É': return true;
                case 'Ê': return true;
                case 'È': return true;
                case 'Ë': return true;
                case 'Í': return true;
                case 'Ì': return true;
                case 'Î': return true;
                case 'Ï': return true;
                case 'Õ': return true;
                case 'Ó': return true;
                case 'Ò': return true;
                case 'Ô': return true;
                case 'Ö': return true;
                case 'Ú': return true;
                case 'Ù': return true;
                case 'Û': return true;
                case 'Ü': return true;
                case 'Ç': return true;
                case 'Ñ': return true;
                default: break;
            }
        }
        return false;
    }
    
    public boolean verificaSeTemNumero(String s){
        if(s.equals(""))return false;
        String mai = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(mai.charAt(i)){
                case '0': return true;
                case '1': return true;
                case '2': return true;
                case '3': return true;
                case '4': return true;
                case '5': return true;
                case '6': return true;
                case '7': return true;
                case '8': return true;
                case '9': return true;
                default: break;
            }
        }
        return false;
    }
    
    public boolean verificaSeTemLetra(String s){
        if(s.equals(""))return false;
        String mai = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(mai.charAt(i)){
                case 'A': return true;
                case 'B': return true;
                case 'C': return true;
                case 'D': return true;
                case 'E': return true;
                case 'F': return true;
                case 'G': return true;
                case 'H': return true;
                case 'I': return true;
                case 'J': return true;
                case 'K': return true;
                case 'L': return true;
                case 'M': return true;
                case 'N': return true;
                case 'O': return true;
                case 'P': return true;
                case 'Q': return true;
                case 'R': return true;
                case 'S': return true;
                case 'T': return true;
                case 'U': return true;
                case 'V': return true;
                case 'W': return true;
                case 'X': return true;
                case 'Y': return true;
                case 'Z': return true;                
                default: break;
            }
        }
        return false;
    }
    
    public final boolean verificaSeTemCharEspeciaisQueOBancoNaoSuporta(String s){
        if(s.equals(""))return false;
        String mai = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(mai.charAt(i)){
                case 'Ã': return true;
                case 'Á': return true;
                case 'À': return true;
                case 'Â': return true;
                case 'Ä': return true;
                case 'É': return true;
                case 'Ê': return true;
                case 'È': return true;
                case 'Ë': return true;
                case 'Í': return true;
                case 'Ì': return true;
                case 'Î': return true;
                case 'Ï': return true;
                case 'Õ': return true;
                case 'Ó': return true;
                case 'Ò': return true;
                case 'Ô': return true;
                case 'Ö': return true;
                case 'Ú': return true;
                case 'Ù': return true;
                case 'Û': return true;
                case 'Ü': return true;
                case 'Ç': return true;
                case 'Ñ': return true;
                case '\'': return true;
                case '?': return true;
                case '!': return true;
                case '\\': return true;
                case '/': return true;
                default: break;
            }
        }
        return false;
    }
    
    public final boolean verificaSeTemCharEspeciais(String s){
        if(s.equals(""))return false;
        String mai = s.toUpperCase();
        for(int i = 0;i<s.length();i++){
            switch(mai.charAt(i)){
                case '-': return true;
                case ':': return true;
                case ',': return true;
                case '.': return true;
                case '*': return true;
                case '(': return true;
                case ')': return true;
                case '{': return true;
                case '}': return true;
                case '%': return true;
                case '$': return true;
                case '#': return true;
                case '@': return true;
                case '!': return true;
                case '?': return true;
                case '=': return true;
                case '&': return true;
                case '/': return true;
                case '+': return true;                
                default: break;
            }
        }
        return false;
    }
    
    public final String verificaELimpaCharEspeciaisNome(String s){
        if(s.equals(""))return "";
        s = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A': aux = aux+s.charAt(i); break;
                case 'B': aux = aux+s.charAt(i); break;
                case 'C': aux = aux+s.charAt(i); break;
                case 'D': aux = aux+s.charAt(i); break;
                case 'E': aux = aux+s.charAt(i); break;
                case 'F': aux = aux+s.charAt(i); break;
                case 'G': aux = aux+s.charAt(i); break;
                case 'H': aux = aux+s.charAt(i); break;
                case 'I': aux = aux+s.charAt(i); break;
                case 'J': aux = aux+s.charAt(i); break;
                case 'K': aux = aux+s.charAt(i); break;
                case 'L': aux = aux+s.charAt(i); break;
                case 'M': aux = aux+s.charAt(i); break;
                case 'N': aux = aux+s.charAt(i); break;
                case 'O': aux = aux+s.charAt(i); break;
                case 'P': aux = aux+s.charAt(i); break;
                case 'Q': aux = aux+s.charAt(i); break;
                case 'R': aux = aux+s.charAt(i); break;
                case 'S': aux = aux+s.charAt(i); break;
                case 'T': aux = aux+s.charAt(i); break;
                case 'U': aux = aux+s.charAt(i); break;
                case 'V': aux = aux+s.charAt(i); break;
                case 'W': aux = aux+s.charAt(i); break;
                case 'X': aux = aux+s.charAt(i); break;
                case 'Y': aux = aux+s.charAt(i); break;
                case 'Z': aux = aux+s.charAt(i); break;
                case '0': aux = aux+s.charAt(i); break;
                case '1': aux = aux+s.charAt(i); break;
                case '2': aux = aux+s.charAt(i); break;
                case '3': aux = aux+s.charAt(i); break;
                case '4': aux = aux+s.charAt(i); break;
                case '5': aux = aux+s.charAt(i); break;
                case '6': aux = aux+s.charAt(i); break;
                case '7': aux = aux+s.charAt(i); break;
                case '8': aux = aux+s.charAt(i); break;
                case '9': aux = aux+s.charAt(i); break;
                case ' ': aux = aux+s.charAt(i); break;
                case '-': aux = aux+s.charAt(i); break;
                case 'Ã': aux = aux+'A'; break;
                case 'Á': aux = aux+'A'; break;
                case 'À': aux = aux+'A'; break;
                case 'Â': aux = aux+'A'; break;
                case 'Ä': aux = aux+'A'; break;
                case 'É': aux = aux+'E'; break;
                case 'Ê': aux = aux+'E'; break;
                case 'È': aux = aux+'E'; break;
                case 'Ë': aux = aux+'E'; break;
                case 'Í': aux = aux+'I'; break;
                case 'Ì': aux = aux+'I'; break;
                case 'Î': aux = aux+'I'; break;
                case 'Ï': aux = aux+'I'; break;
                case 'Õ': aux = aux+'O'; break;
                case 'Ó': aux = aux+'O'; break;
                case 'Ò': aux = aux+'O'; break;
                case 'Ô': aux = aux+'O'; break;
                case 'Ö': aux = aux+'O'; break;
                case 'Ú': aux = aux+'U'; break;
                case 'Ù': aux = aux+'U'; break;
                case 'Û': aux = aux+'U'; break;
                case 'Ü': aux = aux+'U'; break;
                case 'Ç': aux = aux+'C'; break;
                case 'Ñ': aux = aux+'N'; break;
                default: break;
            }
        }
        return aux;
    }
    
    public String verificaELimpaCharEspeciaisPlaca(String s){
        if(s.equals(""))return "";
        s = s.toUpperCase();
        String aux = "";
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A': aux = aux+s.charAt(i); break;
                case 'B': aux = aux+s.charAt(i); break;
                case 'C': aux = aux+s.charAt(i); break;
                case 'D': aux = aux+s.charAt(i); break;
                case 'E': aux = aux+s.charAt(i); break;
                case 'F': aux = aux+s.charAt(i); break;
                case 'G': aux = aux+s.charAt(i); break;
                case 'H': aux = aux+s.charAt(i); break;
                case 'I': aux = aux+s.charAt(i); break;
                case 'J': aux = aux+s.charAt(i); break;
                case 'K': aux = aux+s.charAt(i); break;
                case 'L': aux = aux+s.charAt(i); break;
                case 'M': aux = aux+s.charAt(i); break;
                case 'N': aux = aux+s.charAt(i); break;
                case 'O': aux = aux+s.charAt(i); break;
                case 'P': aux = aux+s.charAt(i); break;
                case 'Q': aux = aux+s.charAt(i); break;
                case 'R': aux = aux+s.charAt(i); break;
                case 'S': aux = aux+s.charAt(i); break;
                case 'T': aux = aux+s.charAt(i); break;
                case 'U': aux = aux+s.charAt(i); break;
                case 'V': aux = aux+s.charAt(i); break;
                case 'W': aux = aux+s.charAt(i); break;
                case 'X': aux = aux+s.charAt(i); break;
                case 'Y': aux = aux+s.charAt(i); break;
                case 'Z': aux = aux+s.charAt(i); break;
                case '0': aux = aux+s.charAt(i); break;
                case '1': aux = aux+s.charAt(i); break;
                case '2': aux = aux+s.charAt(i); break;
                case '3': aux = aux+s.charAt(i); break;
                case '4': aux = aux+s.charAt(i); break;
                case '5': aux = aux+s.charAt(i); break;
                case '6': aux = aux+s.charAt(i); break;
                case '7': aux = aux+s.charAt(i); break;
                case '8': aux = aux+s.charAt(i); break;
                case '9': aux = aux+s.charAt(i); break;
                case 'Ã': aux = aux+'A'; break;
                case 'Á': aux = aux+'A'; break;
                case 'À': aux = aux+'A'; break;
                case 'Â': aux = aux+'A'; break;
                case 'Ä': aux = aux+'A'; break;
                case 'É': aux = aux+'E'; break;
                case 'Ê': aux = aux+'E'; break;
                case 'È': aux = aux+'E'; break;
                case 'Ë': aux = aux+'E'; break;
                case 'Í': aux = aux+'I'; break;
                case 'Ì': aux = aux+'I'; break;
                case 'Î': aux = aux+'I'; break;
                case 'Ï': aux = aux+'I'; break;
                case 'Õ': aux = aux+'O'; break;
                case 'Ó': aux = aux+'O'; break;
                case 'Ò': aux = aux+'O'; break;
                case 'Ô': aux = aux+'O'; break;
                case 'Ö': aux = aux+'O'; break;
                case 'Ú': aux = aux+'U'; break;
                case 'Ù': aux = aux+'U'; break;
                case 'Û': aux = aux+'U'; break;
                case 'Ü': aux = aux+'U'; break;
                case 'Ç': aux = aux+'C'; break;
                case 'Ñ': aux = aux+'N'; break;
                default: break;
            }
        }
        return aux;
    }
    
    public boolean verificaSeCampoCPFCNPJEValido(String cpf, boolean iscpf){
        if(iscpf){
            if(!cpf.equalsIgnoreCase("   .   .   -  ") && !cpf.equals("")){
                return true;
            }else{
                return false;
            }
        }else{
            if(!cpf.equalsIgnoreCase("  .   .   /    -  ") && !cpf.equals("")){
                return true;
            }else{
                return false;
            }
        }
    }
    
    public boolean verificaSeECep(String cep){
        if(cep == null){
            return false;
        }else{
            if(!cep.equals("")){
                if(!cep.equals("     -   ")){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
    
    public boolean verificaSeELetrasSemEspaco(String s){
        if(s.equals(""))return false;
        s = s.toUpperCase();
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A':  break;
                case 'B':  break;
                case 'C':  break;
                case 'D':  break;
                case 'E':  break;
                case 'F':  break;
                case 'G':  break;
                case 'H':  break;
                case 'I':  break;
                case 'J':  break;
                case 'K':  break;
                case 'L':  break;
                case 'M':  break;
                case 'N':  break;
                case 'O':  break;
                case 'P':  break;
                case 'Q':  break;
                case 'R':  break;
                case 'S':  break;
                case 'T':  break;
                case 'U':  break;
                case 'V':  break;
                case 'W':  break;
                case 'X':  break;
                case 'Y':  break;
                case 'Z':  break;
                default: return false;
            }
        }
        return true;
    }
    
    public boolean verificaSeELetrasComEspaco(String s){
        if(s.equals(""))return false;
        s = s.toUpperCase();
        for(int i = 0;i<s.length();i++){
            switch(s.charAt(i)){
                case 'A':  break;
                case 'B':  break;
                case 'C':  break;
                case 'D':  break;
                case 'E':  break;
                case 'F':  break;
                case 'G':  break;
                case 'H':  break;
                case 'I':  break;
                case 'J':  break;
                case 'K':  break;
                case 'L':  break;
                case 'M':  break;
                case 'N':  break;
                case 'O':  break;
                case 'P':  break;
                case 'Q':  break;
                case 'R':  break;
                case 'S':  break;
                case 'T':  break;
                case 'U':  break;
                case 'V':  break;
                case 'W':  break;
                case 'X':  break;
                case 'Y':  break;
                case 'Z':  break;
                default: return false;
            }
        }
        return true;
    }
    
    public String getFirstString(String s){
        String aux = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '-')break;
            aux += s.charAt(i);
        }
        return aux.trim();
    }
    
    public String getSecondString(String s){
        String aux = "";
        boolean teste = false;
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '-'){
                teste = true;
                continue;
            }
            if(teste){
                aux += s.charAt(i);
            }
        }
        return aux.trim();
    }
    
    public String getSecondStringModelo(String s){
        String aux = "";
        boolean teste = false;
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '-'){
                if(i+1 != s.length()){
                    if(s.charAt(i+1) == ' '){
                        i++;
                        teste = true;
                        continue;
                    }
                }
            }
            if(teste){
                aux += s.charAt(i);
            }
        }
        return aux.trim();
    }
    
    public String getFirstStringModelo(String s){
        String aux = "";
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '-'){
                if(i+1 != s.length()){
                    if(s.charAt(i+1) == ' '){
                        break;
                    }
                }
            }
            aux += s.charAt(i);
        }
        return aux.trim();
    }
    
    public boolean isEmptyOrNull(String s){
        return isNullOrEmpty(s);
    }
    
    public boolean isNullOrEmpty(String s){
        if(s == null){
            return true;
        }else{
            if(s.equalsIgnoreCase("")){
                return true;
            }else{
                return false;
            }
        }
    }
}
