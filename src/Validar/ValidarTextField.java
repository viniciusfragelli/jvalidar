/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar;

/**
 *
 * @author vinicius
 */
public interface ValidarTextField {
    
    public abstract int getMinLength();
    public abstract int getMaxLength();
    public abstract boolean isNumero();
    public abstract boolean isValorComercial();
    public abstract float getValorComercialMax();
    public abstract float getValorComercialMin();
    public abstract float getValorMax();
    public abstract float getValorMin();
    public abstract boolean aceptedNullable();
    public abstract boolean isEmail();
    public abstract boolean isCEP();
    
}
