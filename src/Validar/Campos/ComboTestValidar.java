/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar.Campos;

import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import Validar.TestValidar;
import Validar.ValidarCombo;
import Validar.ValidarTextField;

/**
 *
 * @author vinicius
 */
public class ComboTestValidar extends JComboBox implements TestValidar{
    
    private ValidarCombo valider;

    public ComboTestValidar(ValidarCombo valider) {
        super();
        this.valider = valider;
    }

    @Override
    public boolean valida() {
        if(!getSelectedItem().toString().equalsIgnoreCase(valider.getNotAceptedString())){
            setForeground(null);
            return true;
        }else{
            JOptionPane.showMessageDialog(null, "Selecione uma opção!");
            setForeground(Color.red);
            requestFocus();
            return false;
        }
    }
    
}
