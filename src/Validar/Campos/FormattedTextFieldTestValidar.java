/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar.Campos;

import Validar.Funcoes;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import Validar.TestValidar;
import Validar.ValidarFormattedTextField;

/**
 *
 * @author vinicius
 */
public class FormattedTextFieldTestValidar extends JFormattedTextField implements TestValidar{

    private ValidarFormattedTextField valider;

    public FormattedTextFieldTestValidar(ValidarFormattedTextField valider) {
        this.valider = valider;
    }
    
    @Override
    public boolean valida() {
        switch(valider.tipoValidacao()){
            case "DATA": 
                if(Funcoes.getInstance().verificaIsDataValida(getText())){
                    setBorder(BorderFactory.createLineBorder(Color.GRAY));
                    return true;
                }else{
                    JOptionPane.showMessageDialog(null, "Data invalida!");
                    setBorder(BorderFactory.createLineBorder(Color.red));
                    return false;
                }
            case "DATA NULL": 
                if(getText().equalsIgnoreCase("  /  /    ")){
                    return true;
                }else{
                    if(Funcoes.getInstance().verificaIsDataValida(getText())){
                        setBorder(BorderFactory.createLineBorder(Color.GRAY));
                        return true;
                    }else{
                        JOptionPane.showMessageDialog(null, "Data invalida!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }
                }
            case "CPF":
                if(getText().equals("")){
                    JOptionPane.showMessageDialog(null, "CPF invalido!");
                    setBorder(BorderFactory.createLineBorder(Color.red));
                    return false;
                }else{
                    if(getText().equals("   .   .   -  ")){
                        JOptionPane.showMessageDialog(null, "CPF invalido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }else{
                        return true;
                    }
                }
            case "RG":
                if(getText().equals("")){
                    JOptionPane.showMessageDialog(null, "RG invalido!");
                    setBorder(BorderFactory.createLineBorder(Color.red));
                    return false;
                }else{
                    if(getText().equals("       - ")){
                        JOptionPane.showMessageDialog(null, "RG invalido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }else{
                        return true;
                    }
                }
            case "Telefone":
                if(getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Telefone invalido!");
                    setBorder(BorderFactory.createLineBorder(Color.red));
                    return false;
                }else{
                    if(getText().equals("(  )     -    ")){
                        JOptionPane.showMessageDialog(null, "Telefone invalido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }else{
                        return true;
                    }
                }
            case "Celular":
                if(getText().equals("")){
                    JOptionPane.showMessageDialog(null, "Celular invalido!");
                    setBorder(BorderFactory.createLineBorder(Color.red));
                    return false;
                }else{
                    if(getText().equals("(  )      -    ")){
                        JOptionPane.showMessageDialog(null, "Celular invalido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }else{
                        return true;
                    }
                }
            case "CEP":
                if(getText().equals("")){
                    JOptionPane.showMessageDialog(null, "CEP invalido!");
                    setBorder(BorderFactory.createLineBorder(Color.red));
                    return false;
                }else{
                    if(getText().equals("     -   ")){
                        JOptionPane.showMessageDialog(null, "CEP invalido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }else{
                        return true;
                    }
                }
            default: return false;
        }
    }
    
}
