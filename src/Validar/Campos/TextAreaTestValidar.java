/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar.Campos;

import Validar.Funcoes;
import Validar.TestValidar;
import Validar.ValidarFormattedTextField;
import Validar.ValidarTextArea;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 *
 * @author vinicius
 */
public class TextAreaTestValidar extends JTextArea implements TestValidar{

    private ValidarTextArea valider;

    public TextAreaTestValidar(ValidarTextArea valider) {
        this.valider = valider;
    }

    @Override
    public boolean valida() {
        String text = getText();
        if(valider.aceptedNullable()){
            if(getText().equalsIgnoreCase("")){
                return true;
            }
        }
        setText(Funcoes.getInstance().verificaELimpaCharEspeciais(getText()));
        if(valider.getMinLength() != -1){
            if(text.length() < valider.getMinLength()){
                JOptionPane.showMessageDialog(null, "O texto é muito curto!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        return true;
    }
    
}
