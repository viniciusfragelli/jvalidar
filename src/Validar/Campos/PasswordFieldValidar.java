/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar.Campos;

import Validar.Funcoes;
import Validar.TestValidar;
import Validar.ValidarPasswordField;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

/**
 *
 * @author vinicius
 */
public class PasswordFieldValidar extends JPasswordField implements TestValidar{

    private ValidarPasswordField validar;

    public PasswordFieldValidar(ValidarPasswordField validar) {
        this.validar = validar;
    }
    
    @Override
    public boolean valida() {
        String text = new String(getPassword());
        if(Funcoes.getInstance().verificaSeTemCharEspeciaisQueOBancoNaoSuporta(text)){
            JOptionPane.showMessageDialog(null, "A senha não deve ter caracteres como ÃÁÀÂÄÉÊÈËÍÌÎÏÕÓÒÔÖÚÙÛÜÇÑ\\?!\'/");
            setBorder(BorderFactory.createLineBorder(Color.red));
            return false;
        }
        if(validar.isObrigatorioTerCaracteresEspeciais()){
            if(!Funcoes.getInstance().verificaSeTemCharEspeciais(text)){
                JOptionPane.showMessageDialog(null, "Senha tem que ter caracteres especiais!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(validar.naoPodeTerCaracteresEspeciais()){
            if(Funcoes.getInstance().verificaSeTemCharEspeciais(text)){
                JOptionPane.showMessageDialog(null, "Senha não pode ter caracteres especiais!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(validar.isObrigatorioTerLetra()){
            if(!Funcoes.getInstance().verificaSeTemLetra(text)){
                JOptionPane.showMessageDialog(null, "Senha tem que ter pelo menos uma letra!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        
        if(validar.naoPodeTerLetra()){
            if(Funcoes.getInstance().verificaSeTemLetra(text)){
                JOptionPane.showMessageDialog(null, "Senha não pode ter letra!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(validar.isObrigatorioTerNumero()){
            if(!Funcoes.getInstance().verificaSeTemNumero(text)){
                JOptionPane.showMessageDialog(null, "Senha tem que ter pelo menos um numero!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(validar.naoPodeTerNumero()){
            if(Funcoes.getInstance().verificaSeTemNumero(text)){
                JOptionPane.showMessageDialog(null, "Senha não pode ter numeros!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(validar.isObrigatorioTerLetraMaiuscula()){
            if(!Funcoes.getInstance().verificaSeTemLetraMaiuscula(text)){
                JOptionPane.showMessageDialog(null, "Senha tem que ter pelo menos uma letra maiuscula!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }        
        if(validar.naoPodeTerLetraMaiuscula()){
            if(Funcoes.getInstance().verificaSeTemLetraMaiuscula(text)){
                JOptionPane.showMessageDialog(null, "Senha não pode ter letra maiuscula!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(validar.numeroMinimoDeCaracteres() != -1){
            if(validar.numeroMinimoDeCaracteres() > text.length()){
                JOptionPane.showMessageDialog(null, "Senha tem que ter no minimo "+validar.numeroMinimoDeCaracteres()+"!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(validar.numeroMaximoDeCaracteres() != -1){
            if(validar.numeroMaximoDeCaracteres() < text.length()){
                JOptionPane.showMessageDialog(null, "Senha tem que ter no máximo "+validar.numeroMaximoDeCaracteres()+"!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        setBorder(BorderFactory.createLineBorder(Color.GRAY));
        return true;
    }
    
}
