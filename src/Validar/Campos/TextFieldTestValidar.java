/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar.Campos;

import Validar.Funcoes;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import Validar.TestValidar;
import Validar.ValidarTextField;

/**
 *
 * @author vinicius
 */
public class TextFieldTestValidar extends JTextField implements TestValidar{

    private ValidarTextField valider;
    
    public TextFieldTestValidar(ValidarTextField valider) {
        super();
        this.valider = valider;
    }
    
    @Override
    public boolean valida() {
        setText(Funcoes.getInstance().verificaELimpaCharEspeciais(getText()));
        String text = getText();
        if(valider.getMinLength() != -1){
            if(text.length() < valider.getMinLength()){
                JOptionPane.showMessageDialog(null, "O texto é muito curto!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(valider.getMaxLength() != -1){
            if(text.length() > valider.getMaxLength()){
                JOptionPane.showMessageDialog(null, "O texto é muito longo!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(valider.isNumero()){
            if(valider.aceptedNullable()){
                if(text.equalsIgnoreCase("")){
                    return true;
                }
            }
            if(Funcoes.getInstance().verificaIsNumero(text)){
                if(valider.getValorMin() != -1){
                int valor = Integer.parseInt(text);
                    if(valider.getValorMin() > valor){
                        JOptionPane.showMessageDialog(null, "Valor abaixo do permitido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }
                }
                if(valider.getValorMax() != -1){
                int valor = Integer.parseInt(text);
                    if(valider.getValorMax() < valor){
                        JOptionPane.showMessageDialog(null, "Valor é acima do permitido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }
                }
            }else{
                JOptionPane.showMessageDialog(null, "Valor não é valido!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(valider.isValorComercial()){
            if(valider.aceptedNullable()){
                if(text.equalsIgnoreCase("")){
                    return true;
                }
            }
            if(Funcoes.getInstance().verificaIsValorComercialComVirgulaValido(text)){
                float valor2 = Funcoes.getInstance().formataValorEParseFloat(text);
                if(valider.getValorMin() != -1){
                    if(valider.getValorComercialMin() > valor2){
                        JOptionPane.showMessageDialog(null, "Valor é abaixo do permitido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }
                }
                if(valider.getValorMax() != -1){
                    if(valider.getValorComercialMax() < valor2){
                        JOptionPane.showMessageDialog(null, "Valor o é acima do permitido!");
                        setBorder(BorderFactory.createLineBorder(Color.red));
                        return false;
                    }
                }
            }else{
                JOptionPane.showMessageDialog(null, "Valor não é valido!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(valider.isEmail()){
            if(valider.aceptedNullable()){
                if(text.equals("")){
                    return true;
                }
            }
            if(Funcoes.getInstance().verificaSeEmailEValido(text)){
                return true;
            }else{
                JOptionPane.showMessageDialog(null, "Email não é valido!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;
            }
        }
        if(valider.isCEP()){
            if(valider.aceptedNullable()){
                if(text.equals("")){
                    return true;
                }
            }
            String cep = text.replace("-", "");
            if(Funcoes.getInstance().verificaIsNumero(cep)){
                if(cep.length() == 8){
                    return true;
                }else{
                    JOptionPane.showMessageDialog(null, "CEP não é valido!");
                    setBorder(BorderFactory.createLineBorder(Color.red));
                    return false;
                }
            }else{
                JOptionPane.showMessageDialog(null, "CEP não é valido!");
                setBorder(BorderFactory.createLineBorder(Color.red));
                return false;                
            }
        }
        setBorder(BorderFactory.createLineBorder(Color.GRAY));
        return true;
    }
    
}
