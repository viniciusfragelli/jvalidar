/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar.PasswordField;

import Validar.ValidarPasswordField;

/**
 *
 * @author vinicius
 */
public class SimplePasswordField implements ValidarPasswordField{

    @Override
    public boolean isObrigatorioTerLetraMaiuscula() {
        return false;
    }

    @Override
    public boolean isObrigatorioTerLetra() {
        return true;
    }

    @Override
    public boolean isObrigatorioTerNumero() {
        return true;
    }

    @Override
    public boolean isObrigatorioTerCaracteresEspeciais() {
        return false;
    }

    @Override
    public int numeroMinimoDeCaracteres() {
        return 8;
    }

    @Override
    public int numeroMaximoDeCaracteres() {
        return -1;
    }

    @Override
    public boolean naoPodeTerLetraMaiuscula() {
        return false;
    }

    @Override
    public boolean naoPodeTerLetra() {
        return false;
    }

    @Override
    public boolean naoPodeTerNumero() {
        return false;
    }

    @Override
    public boolean naoPodeTerCaracteresEspeciais() {
        return true;
    }
    
}
