/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validar.TextField;

import Validar.ValidarTextField;

/**
 *
 * @author vinicius
 */
public class NumeroNullTextFieldValidar implements ValidarTextField{
    
    @Override
    public boolean isEmail() {
        return false;
    }

    @Override
    public int getMinLength() {
        return -1;
    }

    @Override
    public int getMaxLength() {
        return -1;
    }

    @Override
    public boolean isNumero() {
        return true;
    }

    @Override
    public boolean isValorComercial() {
        return false;
    }

    @Override
    public float getValorComercialMax() {
        return -1;
    }

    @Override
    public float getValorComercialMin() {
        return -1;
    }

    @Override
    public float getValorMax() {
        return -1;
    }

    @Override
    public float getValorMin() {
        return -1;
    }

    @Override
    public boolean aceptedNullable() {
        return true;
    }

    @Override
    public boolean isCEP() {
        return false;
    }
    
}
